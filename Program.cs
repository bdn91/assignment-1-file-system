﻿using System;
using System.IO;
using System.Linq;
using Helpers;

namespace assignment_1_file_system
{
    class Program
    {
        static string resourceDir = "resources/";

        static void Main(string[] args)
        {
            HomePrompt();

            bool validNumber = true;

            do
            {
                string option = Console.ReadLine();

                validNumber = MenuPrompt(option);

            } while (!validNumber);
        }

        static void ShowFiles()
        {
            var dirInfo = new DirectoryInfo(resourceDir);
            FileInfo[] fileInfos = dirInfo.GetFiles("*");
            var files = fileInfos.OrderBy(f => f.Name, new AlphanumComparator());
            foreach (FileInfo file in files)
            {
                Console.WriteLine(file.Name);
            }
        }

        static void HomePrompt()
        {
            Console.WriteLine("\nWelcome to file system manager console!\n");
            Console.WriteLine("1. Show all files in directory");
            Console.WriteLine("2. Show files by extension");
            Console.WriteLine("3. Manipulate Dracula.txt");
            Console.WriteLine("\nChoose an option:");
        }

        static bool MenuPrompt(string optionNumber)
        {
            switch (optionNumber)
            {
                case "1":
                    Console.WriteLine("Showing all files:");
                    ShowFiles();
                    break;
                case "2":
                    Console.WriteLine("Number 2 was choosen");
                    break;
                case "3":
                    Console.WriteLine("Number 3 was choosen");
                    break;
                default:
                    Console.WriteLine("You've entered an invalid number! Try again");
                    return false;
            }
            return true;
        }
    }
}
